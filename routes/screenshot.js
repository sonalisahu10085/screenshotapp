var express = require('express');
var router = express.Router();
var puppeteer = require('puppeteer');

const generateScreenshot = async ({url,width,height}) => {
    const browser = await puppeteer.launch({headless : 'new'});
    const page = await browser.newPage();
    await page.goto(url);
    await page.setViewport({width,height});
    const bufferData = await page.screenshot();
    await browser.close();
    return bufferData;
  }

/* GET screenshot page. */

router.get('/', async function(req, res, next) {
const {query} = req;
let { url, w, h} = query;
// const { url} = query;

if(!url){
    res.send("Url is missing");
    return;
}

if(!w && !h){
    w = 390;
    h = 844;
}

// url = url.includes('https://') ? url : `https://${url}`;
const surl = url.includes('https://') ? url : `https://${url}`;
const data = await generateScreenshot({url : surl, width : (+w), height : (+h)});
res.set('Content-Type','image/png');
res.send(data);
});

module.exports = router;